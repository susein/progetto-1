
function elementInViewport(el) {
	var top = el.offsetTop;
	var left = el.offsetLeft;
	var width = el.offsetWidth;
	var height = el.offsetHeight;
  
	while(el.offsetParent) {
	  el = el.offsetParent;
	  top += el.offsetTop;
	  left += el.offsetLeft;
	}
  
	return (
		top < (window.pageYOffset + window.innerHeight) &&
		left < (window.pageXOffset + window.innerWidth) &&
		(top + height) > window.pageYOffset &&
		(left + width) > window.pageXOffset
	  );
  }

function mapRange(value, low1, high1, low2, high2) {
	return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

window.addEventListener("scroll",function(){
	// const scroll = document.documentElement.scrollTop || document.body.scrollTop
	// const fullw = window.innerHeight
	// const header_height = 182
	// const w = fullw - header_height

	// if ( scroll > w ) {
	// 	const navbar   = document.querySelector('.navbar')
	// 	const container = document.querySelector('.main_container')
	// 	navbar.classList.add('compact')
	// 	container.classList.add('compact')
	// } else {
	// 	const navbar = document.querySelector('.navbar')
	// 	const container = document.querySelector('.main_container')
	// 	navbar.classList.remove('compact')
	// 	container.classList.remove('compact')
	// }

	

	// const gallery_images = document.querySelectorAll('.gallery_shape');
	
	// gallery_images.forEach(function(el) {
	// 	// do whatever
	// 	 if ( elementInViewport(el) ) {
	// 	 	el.classList.add('loaded')
	// 	 } else {
	// 	 	console.log('not visibile')
	// 	 }
	//   });

	const header = document.querySelector('.navbar')
	const headerBounding = header.getBoundingClientRect()
	const gallery_images = document.querySelectorAll('.gallery_shape')
	const scroll = document.documentElement.scrollTop || document.body.scrollTop

	if(scroll >= window.innerHeight - headerBounding.height) {
		header.classList.add('compact')
	}else {
		header.classList.remove('compact')
	}

	gallery_images.forEach((image) => {
		if(image.getBoundingClientRect().top < window.innerHeight - (image.getBoundingClientRect().height / 6)) {
			image.classList.add('loaded')
		}
	})
})


window.addEventListener('load', function() {


	const about_image = document.querySelector('.about_image')

	if ( about_image ) {
		about_image.addEventListener('mousemove',function(e){
			 console.log(e)
		})

		about_image.addEventListener('mouseleave',function(e){
			 console.log(e)
		})
	}





	const cta = document.querySelector('.container_arrow')

	if ( cta ) {
		cta.addEventListener("click", function(){
			gallery.scrollIntoView({behavior: "smooth"})
		})
	}

	const gallery = document.querySelector('.gallery')

	if ( gallery ) {
		var elem = document.querySelector('.grid');
		var msnry = new Masonry( elem, {
		  // options
		  itemSelector: '.grid-item',
		  columnWidth: '.grid-sizer',
	  	  percentPosition: true
		});	
	}
	
	

})